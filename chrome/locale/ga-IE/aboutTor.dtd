<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Maidir le Tor">

<!ENTITY aboutTor.viewChangelog.label "Féach ar an Logchomhad Athruithe">

<!ENTITY aboutTor.ready.label "Brabhsáil. Príobháideachas.">
<!ENTITY aboutTor.ready2.label "Tá tú réidh don bhrabhsálaí is príobháidí ar domhan.">
<!ENTITY aboutTor.failure.label "Chuaigh rud éigin ar strae!">
<!ENTITY aboutTor.failure2.label "Níl Tor ag obair sa bhrabhsálaí seo.">

<!ENTITY aboutTor.search.label "Cuardach DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Ceisteanna?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Féach ar Lámhleabhar Bhrabhsálaí Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Lámhleabhar Bhrabhsálaí Tor">

<!ENTITY aboutTor.tor_mission.label "Eagraíocht neamhbhrabúis 501(c)(3) sna Stáit Aontaithe is ea Tionscadal Tor. An phríomhaidhm atá againn ná cearta daonna agus an tsaoirse a chur chun cinn trí theicneolaíochtaí oscailte agus saor in aisce a thacaíonn leis an bpríobháideachas ar líne a fhorbairt, na teicneolaíochtaí seo a scaipeadh gan srian agus a n-úsáid a spreagadh, agus tuiscint níos fearr ar chúrsaí príobháideachais a fhorbairt i measc an phobail.">
<!ENTITY aboutTor.getInvolved.label "Glac Páirt »">

<!ENTITY aboutTor.newsletter.tagline "Faigh an nuacht is déanaí maidir le Tor i do bhosca isteach.">
<!ENTITY aboutTor.newsletter.link_text "Cláraigh le Nuachtlitir Tor.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tá Tor saor in aisce a bhuí le bronntanais airgid ó dhaoine cosúil leatsa.">
<!ENTITY aboutTor.donationBanner.buttonA "Tabhair síntiús airgid anois">

<!-- Year end campaign strings -->

<!-- LOCALIZATION NOTE (aboutTor.yec.slogan): This string is written on a protest sign and the translated
  phrase needs to be a short and concise slogan. We would like the phrase to fit on 3 to 5 lines. If a
  translation of 'HANDS OFF MY DATA' cannot be made short, we have provided these alternative slogans
  with a similar theme:

  - DON'T TOUCH MY DATA
  - DON'T SPY ON MY DATA
  - MY DATA IS PRIVATE
  - KEEP OFF MY DATA

  Please place newline characters (\n) between words or phrases which can be placed in separate lines
  so we can word-wrap our final assets correctly.

  Thank you!
-->
<!ENTITY aboutTor.yec.slogan "HANDS\nOFF\nMY\nDATA">
<!ENTITY aboutTor.yec.motto "Is buncheart daonna é an príobháideachas">
<!-- LOCALIZATION NOTE (aboutTor.yec.donationMatch): Please translate the 'Friends of Tor' phrase, but
  also format it like the name of an organization in whichever way that is appropriate for your locale.

  Please keep the currency in USD.

  Thank you!
-->
<!ENTITY aboutTor.yec.donationMatch "Your donation will be matched by Friends of Tor, up to $150,000.">
