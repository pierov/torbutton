# Copyright (c) 2019, The Tor Project, Inc.
# See LICENSE for licensing information.
# vim: set sw=2 sts=2 ts=8 et:

onboarding.tour-tor-welcome=Начало
onboarding.tour-tor-welcome.title=Вы готовы
onboarding.tour-tor-welcome.description=Tor Browser – лучший защитник приватности и безопасности при просмотре сайтов. Теперь вам не страшны слежка и цензура. Подробнее об этом расскажет наше маленькое руководство.
onboarding.tour-tor-welcome.next-button=Приватность

onboarding.tour-tor-privacy=Приватность
onboarding.tour-tor-privacy.title=Слежка останется с носом
onboarding.tour-tor-privacy.description=Tor Browser изолирует файлы cookie и удаляет историю браузера после завершения сессии. Эти сделано ради приватности и безопасности. Перейдите к пункту "Сеть Tor", чтобы узнать, как мы защищаем вас на сетевом уровне.
onboarding.tour-tor-privacy.button=Сеть Tor

onboarding.tour-tor-network=Сеть Tor
onboarding.tour-tor-network.title=Децентрализованная сеть
onboarding.tour-tor-network.description=Tor Browser подключает вас к сети Tor. Ее поддерживают волонтеры со всего мира. Это не VPN, здесь нет единственного узла, отказ которого вызовет сбой всей системы. Нет здесь и централизованного сервиса, которому придется довериться, чтобы приватно использовать интернет.
onboarding.tour-tor-network.description-para2=НОВОЕ: настройки сети Tor, включая возможность запрашивать мосты для стран, где Tor заблокирован.
onboarding.tour-tor-network.action-button=Настройте параметры сети Tor
onboarding.tour-tor-network.button=Просмотр цепочки

onboarding.tour-tor-circuit-display=Просмотр цепочки
onboarding.tour-tor-circuit-display.title=Ваша цепочка
onboarding.tour-tor-circuit-display.description=Для каждого посещаемого вами домена трафик передается и шифруется в цепочке через три узла Tor. Эти узлы разбросаны по всему миру. Ни один веб-сайт не знает, откуда вы подключаетесь к сети. Можно изменить маршрут, если выбрать  в меню пункт "Новая цепочка Tor для этого сайта".
onboarding.tour-tor-circuit-display.button=Ваша цепочка
onboarding.tour-tor-circuit-display.next-button=Безопасность

onboarding.tour-tor-security=Безопасность
onboarding.tour-tor-security.title=Настраивайте для себя
onboarding.tour-tor-security.description=Предлагаем дополнительные настройки безопасности. С их помощью браузер блокирует элементы, которые злоумышленник может использовать для атаки на ваш компьютер. Нажмите кнопку ниже, чтобы увидеть эти настройки.
onboarding.tour-tor-security.description-suffix=Обратите внимание: по умолчанию NoScript и HTTPS Everywhere не отображаются в панели задач, но вы можете изменить это в настройках.
onboarding.tour-tor-security-level.button=Ваш уровень безопасности
onboarding.tour-tor-security-level.next-button=Рекомендации

onboarding.tour-tor-expect-differences=Рекомендации
onboarding.tour-tor-expect-differences.title=Возможны отличия
onboarding.tour-tor-expect-differences.description=Tor обеспечивает безопасность и конфиденциальность, но ценой некоторых изменений. Скорость работы в сети может быть немного ниже. Некоторые элементы сайтов могут не работать или вовсе не загружаться (в зависимости от настроек безопасности). Иногда, вероятно, вам придется лишний раз доказывать, что вы человек, а не робот.
onboarding.tour-tor-expect-differences.button=Посмотреть FAQ
onboarding.tour-tor-expect-differences.next-button=Onion-ресурсы

onboarding.tour-tor-onion-services=Onion-ресурсы
onboarding.tour-tor-onion-services.title=Попробуйте суперзащиту
onboarding.tour-tor-onion-services.description=Onion-ресурсы – сайты, которые заканчиваются на .onion. Они обеспечивают дополнительную защиту своим владельцам и посетителям, в том числе от цензуры. Onion-ресурсы позволяют любому человеку публиковать контент и предлагать свои услуги. Нажмите кнопку ниже, чтобы посетить onion-сайт DuckDuckGo.
onboarding.tour-tor-onion-services.button=Посетите onion-сайт
onboarding.tour-tor-onion-services.next-button=Готово

onboarding.overlay-icon-tooltip-updated2=Что нового\nв %S
onboarding.tour-tor-update.prefix-new=Новое
onboarding.tour-tor-update.prefix-updated=Обновлено

onboarding.tour-tor-toolbar=Панель инструментов
onboarding.tour-tor-toolbar-update-9.0.title=Прощай, кнопка "Onion"
onboarding.tour-tor-toolbar-update-9.0.description=Мы хотим, чтобы в Tor Browser все важное было под рукой.
onboarding.tour-tor-toolbar-update-9.0.description-para2=Кнопка "Onion" больше не нужна. Теперь вы можете увидеть свою цепочку Tor, нажав значок замочка в адресной строке. Создать новую личность можно нажатием кнопки на панели инструментов или через меню [≡].
onboarding.tour-tor-toolbar-update-9.0.button=Как запросить новую личность
onboarding.tour-tor-toolbar-update-9.0.next-button=Перейти в cеть Tor

# Circuit Display onboarding.
onboarding.tor-circuit-display.next=Далее
onboarding.tor-circuit-display.done=Готово
onboarding.tor-circuit-display.one-of-three=1 из 3
onboarding.tor-circuit-display.two-of-three=2 из 3
onboarding.tor-circuit-display.three-of-three=3 из 3

onboarding.tor-circuit-display.intro.title=Как работает цепочка?
onboarding.tor-circuit-display.intro.msg=Цепочка образуется из случайно назначенных узлов. Это компьютеры в разных частях света. Они настроены для пересылки трафика Tor. Цепочка делает ваш браузер безопасным и позволяет подключаться к onion-сайтам.

onboarding.tor-circuit-display.diagram.title=Просмотр цепочки
onboarding.tor-circuit-display.diagram.msg=Здесь показаны узлы, которые составляют цепочку для этого сайта. У каждого сайта своя цепочка. Это нужно, чтобы нельзя было сопоставить ваши действия на разных сайтах.

onboarding.tor-circuit-display.new-circuit.title=Нужна новая цепочка?
onboarding.tor-circuit-display.new-circuit.msg=Если не получается открыть сайт или он не загружается должным образом, попробуйте нажать эту кнопку. Сайт перезагрузится с новой цепочкой.
