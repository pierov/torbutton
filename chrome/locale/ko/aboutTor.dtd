<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Tor에 대해서">

<!ENTITY aboutTor.viewChangelog.label "변경이력 보기">

<!ENTITY aboutTor.ready.label "은밀하게 탐색하십시오.">
<!ENTITY aboutTor.ready2.label "당신은 온 세상에서 가장 은밀하게 탐색의 경험을 할 준비가 되었습니다.">
<!ENTITY aboutTor.failure.label "뭔가 잘못되었습니다!">
<!ENTITY aboutTor.failure2.label "Tor는 이 브라우저에서 작동하지 않습니다.">

<!ENTITY aboutTor.search.label "DuckDuckGo으로 검색">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "질문이 있으신가요?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Tor 브라우저 설명서를 읽어보기 »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Tor 브라우저 설명서">

<!ENTITY aboutTor.tor_mission.label "Tor 프로젝트는 자유와 인권증진에 기여하는 US 501(c)(3) 비영리 단체입니다. 이는 자유롭게 이용 가능하며 오픈 소스 익명성과 사생활 보호기술을 개발하고 그 무제한적으로 이용하도록 지원하여 과학적 공적 이해로 나아갑니다.">
<!ENTITY aboutTor.getInvolved.label "참여하기 »">

<!ENTITY aboutTor.newsletter.tagline "최신의 Tor 뉴스를 받은 편지함에 곧장 받으십시오.">
<!ENTITY aboutTor.newsletter.link_text "Tor 뉴스를 구독.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor는 바로 당신과 같은 사람들의 기부 덕분에 자유롭게 사용할 수 있습니다.">
<!ENTITY aboutTor.donationBanner.buttonA "기부하기">

<!-- Year end campaign strings -->

<!-- LOCALIZATION NOTE (aboutTor.yec.slogan): This string is written on a protest sign and the translated
  phrase needs to be a short and concise slogan. We would like the phrase to fit on 3 to 5 lines. If a
  translation of 'HANDS OFF MY DATA' cannot be made short, we have provided these alternative slogans
  with a similar theme:

  - DON'T TOUCH MY DATA
  - DON'T SPY ON MY DATA
  - MY DATA IS PRIVATE
  - KEEP OFF MY DATA

  Please place newline characters (\n) between words or phrases which can be placed in separate lines
  so we can word-wrap our final assets correctly.

  Thank you!
-->
<!ENTITY aboutTor.yec.slogan "HANDS\nOFF\nMY\nDATA">
<!ENTITY aboutTor.yec.motto "프라이버시는 인권입니다">
<!-- LOCALIZATION NOTE (aboutTor.yec.donationMatch): Please translate the 'Friends of Tor' phrase, but
  also format it like the name of an organization in whichever way that is appropriate for your locale.

  Please keep the currency in USD.

  Thank you!
-->
<!ENTITY aboutTor.yec.donationMatch "Your donation will be matched by Friends of Tor, up to $150,000.">
