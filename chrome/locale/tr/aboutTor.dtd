<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Tor hakkında">

<!ENTITY aboutTor.viewChangelog.label "Değişiklik günlüğünü görüntüle">

<!ENTITY aboutTor.ready.label "Keşfedin. Gizlice.">
<!ENTITY aboutTor.ready2.label "Dünyanın en kişisel web tarama deneyimine hazırsınız.">
<!ENTITY aboutTor.failure.label "Bir şeyler ters gitti!">
<!ENTITY aboutTor.failure2.label "Tor bu tarayıcı ile çalışmıyor.">

<!ENTITY aboutTor.search.label "DuckDuckGo ile arayın">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Sorularınız mı var?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Tor Browser rehberine bakabilirsiniz">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Tor Browser rehberi">

<!ENTITY aboutTor.tor_mission.label "Tor Projesi, Birleşik Devletler 501(c)(3) vergi muafiyeti maddesi kapsamında, özgür ve açık kaynaklı anonimlik ve kişisel gizlilik teknolojileri geliştirerek insan hakları ve özgürlüklerini ileriye götürmek, bu teknolojilerin bilimsel ve kültürel olarak bilinirliğini arttırmak ve herkes tarafından erişebilmesini sağlamak amacıyla çalışan, kar amacı gütmeyen bir kuruluştur.">
<!ENTITY aboutTor.getInvolved.label "Katkıda bulunun »">

<!ENTITY aboutTor.newsletter.tagline "Tor gelişmeleri ile ilgili e-posta almak ister misiniz?">
<!ENTITY aboutTor.newsletter.link_text "Tor duyurularına abone olun">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor sizin gibi insanların bağışları ile desteklendiği için ücretsiz olarak kullanılabiliyor.">
<!ENTITY aboutTor.donationBanner.buttonA "Bağış yapın">

<!-- Year end campaign strings -->

<!-- LOCALIZATION NOTE (aboutTor.yec.slogan): This string is written on a protest sign and the translated
  phrase needs to be a short and concise slogan. We would like the phrase to fit on 3 to 5 lines. If a
  translation of 'HANDS OFF MY DATA' cannot be made short, we have provided these alternative slogans
  with a similar theme:

  - DON'T TOUCH MY DATA
  - DON'T SPY ON MY DATA
  - MY DATA IS PRIVATE
  - KEEP OFF MY DATA

  Please place newline characters (\n) between words or phrases which can be placed in separate lines
  so we can word-wrap our final assets correctly.

  Thank you!
-->
<!ENTITY aboutTor.yec.slogan "VERİLERİME\nDOKUNMA">
<!ENTITY aboutTor.yec.motto "Kişisel gizlilik bir insan hakkıdır">
<!-- LOCALIZATION NOTE (aboutTor.yec.donationMatch): Please translate the 'Friends of Tor' phrase, but
  also format it like the name of an organization in whichever way that is appropriate for your locale.

  Please keep the currency in USD.

  Thank you!
-->
<!ENTITY aboutTor.yec.donationMatch "Friends of Tor, $150.000 altında yaptığınız bağış kadar katkıda bulunacak.">
