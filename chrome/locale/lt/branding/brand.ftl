# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Tor Naršyklė
-brand-short-name = Tor Naršyklė
-brand-full-name = Tor Naršyklė
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Tor Naršyklė
-vendor-short-name = Tor projektas
trademarkInfo = 'Tor' ir 'Onion logotipas' yra registruoti Tor Project, Inc. prekių ženklai.
