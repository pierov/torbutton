<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Acerca de Tor">

<!ENTITY aboutTor.viewChangelog.label "Ver registro de cambios">

<!ENTITY aboutTor.ready.label "Explorá. Privadamente.">
<!ENTITY aboutTor.ready2.label "Estás listo para la experiencia de navegación más privada del mundo.">
<!ENTITY aboutTor.failure.label "¡Algo salió mal!">
<!ENTITY aboutTor.failure2.label "Tor no funciona en este navegador.">

<!ENTITY aboutTor.search.label "Buscá con DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "¿Preguntas?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Revisá nuestro manual del Navegador Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Manual del Navegador Tor">

<!ENTITY aboutTor.tor_mission.label "El proyecto Tor es una organización sin fines de lucro bajo las provisiones de la ley EUA 501(c)(3), cuya misión es avanzar los derechos y libertades humanas creando y desplegando tecnologías de anonimato y privacidad de fuente abierta, soportando su disponibilidad y uso irrestricto, y ampliando su entendimiento científico y popular.">
<!ENTITY aboutTor.getInvolved.label "Involucrate »">

<!ENTITY aboutTor.newsletter.tagline "Recibí las últimas noticias de Tor derecho en tu bandeja de entrada.">
<!ENTITY aboutTor.newsletter.link_text "Registrate en Tor News.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor puede ser usado libremente gracias a las donaciones de personas como vos.">
<!ENTITY aboutTor.donationBanner.buttonA "Doná ahora">

<!-- Year end campaign strings -->

<!-- LOCALIZATION NOTE (aboutTor.yec.slogan): This string is written on a protest sign and the translated
  phrase needs to be a short and concise slogan. We would like the phrase to fit on 3 to 5 lines. If a
  translation of 'HANDS OFF MY DATA' cannot be made short, we have provided these alternative slogans
  with a similar theme:

  - DON'T TOUCH MY DATA
  - DON'T SPY ON MY DATA
  - MY DATA IS PRIVATE
  - KEEP OFF MY DATA

  Please place newline characters (\n) between words or phrases which can be placed in separate lines
  so we can word-wrap our final assets correctly.

  Thank you!
-->
<!ENTITY aboutTor.yec.slogan "NO\nTOQUÉS\nMIS\nDATOS">
<!ENTITY aboutTor.yec.motto "La privacidad es un derecho humano">
<!-- LOCALIZATION NOTE (aboutTor.yec.donationMatch): Please translate the 'Friends of Tor' phrase, but
  also format it like the name of an organization in whichever way that is appropriate for your locale.

  Please keep the currency in USD.

  Thank you!
-->
<!ENTITY aboutTor.yec.donationMatch "Tu donación va a ser igualada por Amigos de Tor hasta USD 150.000">
